package com.ryan.amg.review.processing

import org.springframework.http.MediaType

class TagAccSpec extends AcceptanceTest {

    def "DELETE /api/review/{reviewId}/processing/tags/{tagName} Removes a tag from an existing processing record"() {

        given:
            String uuid = UUID.randomUUID().toString()
            String reviewId = "test-review_${uuid}"
            def reviewProcessingRequest = [
                unprocessedTags: [
                    'Genre_' + uuid,
                    'Score_' + uuid
                ]
            ]

        when: "A review processing record is created"
            def postResponse = restClient.post(
                path: "/api/reviews/${reviewId}/processing",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewProcessingRequest
            )
            def postResponseData = postResponse.data

        then:
            postResponse.status == 201
            postResponseData.reviewId == reviewId
            postResponseData.unprocessedTags.each { assert reviewProcessingRequest.unprocessedTags.contains(it) }

        when: "A tag is removed from the processing record"
            def deleteResponse = restClient.delete(
                path: "/api/reviews/${reviewId}/processing/tags/Genre_${uuid}",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE
            )
            def deleteResponseData = deleteResponse.data

        then:
            deleteResponse.status == 200
            deleteResponseData.reviewId == reviewId
            deleteResponseData.unprocessedTags == ['Score_' + uuid]

        cleanup:
            LOG.info('==================================================')
            LOG.info('Start cleanup')
            LOG.info('==================================================')
            restClient.delete(path: "/api/reviews/${reviewId}/processing")
            LOG.info('==================================================')
            LOG.info('End cleanup')
            LOG.info('==================================================')

    }

}
