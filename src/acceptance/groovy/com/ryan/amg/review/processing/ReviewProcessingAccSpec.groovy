package com.ryan.amg.review.processing

import org.springframework.http.MediaType

class ReviewProcessingAccSpec extends AcceptanceTest {

    def "POST /api/review/{reviewId}/processing Creates a new review processing record"() {

        given:
            String uuid = UUID.randomUUID().toString()
            String reviewId = "test-review_${uuid}"
            def reviewProcessingRequest = [
                unprocessedTags: ['Genre_' + uuid, 'Score_' + uuid]
            ]

        when:
            def response = restClient.post(
                path: "/api/reviews/${reviewId}/processing",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewProcessingRequest
            )
            def actualResponse = response.data

        then:
            response.status == 201
            actualResponse.reviewId == reviewId
            actualResponse.unprocessedTags.each { assert reviewProcessingRequest.unprocessedTags.contains(it) }

        cleanup:
            LOG.info('==================================================')
            LOG.info('Start cleanup')
            LOG.info('==================================================')
            restClient.delete(path: "/api/reviews/${reviewId}/processing")
            LOG.info('**************************************************')
            LOG.info('End cleanup')
            LOG.info('**************************************************')

    }

    def "DELETE /api/reviews/{reviewId}/processing Deletes a review processing record when an existing review id is provided"() {

        given:
            String uuid = UUID.randomUUID().toString()
            String reviewId = "test-review_${uuid}"
            def reviewProcessingRequest = [
                unprocessedTags: ['Genre_' + uuid, 'Score_' + uuid]
            ]

        when:
            def response = restClient.post(
                path: "/api/reviews/${reviewId}/processing",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewProcessingRequest
            )

        then:
            response.status == 201

        when:
            def reviewDeletionResponse = restClient.delete(
                path: "/api/reviews/${reviewId}/processing"
            )

        then:
            reviewDeletionResponse.status == 204

    }

}
