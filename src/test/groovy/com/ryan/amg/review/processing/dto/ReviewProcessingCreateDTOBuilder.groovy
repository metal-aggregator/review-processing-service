package com.ryan.amg.review.processing.dto

class ReviewProcessingCreateDTOBuilder {
    List<String> unprocessedTags = ['tag1', 'tag2']

    ReviewProcessingCreateDTOBuilder setUnprocessedTags(List<String> unprocessedTags) {
        this.unprocessedTags = unprocessedTags
        return this
    }

    ReviewProcessingCreateDTO build() {
        return new ReviewProcessingCreateDTO(
            unprocessedTags: unprocessedTags
        )
    }

}
