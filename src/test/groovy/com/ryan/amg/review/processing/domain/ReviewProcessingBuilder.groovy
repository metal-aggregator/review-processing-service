package com.ryan.amg.review.processing.domain

import com.ryan.amg.review.processing.db.entity.ReviewProcessingEntity

class ReviewProcessingBuilder {
    String reviewId = 'id-123'
    List<String> unprocessedTags = ['tag1', 'tag2']

    ReviewProcessingBuilder withReviewId(String reviewId) {
        this.reviewId = reviewId
        return this
    }

    ReviewProcessingBuilder withUnprocessedTags(List<String> unprocessedTags) {
        this.unprocessedTags = unprocessedTags
        return this
    }

    ReviewProcessingBuilder fromReviewProcessingEntity(ReviewProcessingEntity reviewProcessingEntity) {
        this.reviewId = reviewProcessingEntity.getReviewId()
        this.unprocessedTags = reviewProcessingEntity.getUnprocessedTags().collect()
        return this
    }

    ReviewProcessing build() {
        return new ReviewProcessing(
            reviewId: reviewId,
            unprocessedTags: unprocessedTags
        )
    }

}
