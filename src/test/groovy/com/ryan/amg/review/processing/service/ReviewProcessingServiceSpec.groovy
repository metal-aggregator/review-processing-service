package com.ryan.amg.review.processing.service

import com.ryan.amg.review.processing.db.entity.ReviewProcessingEntityBuilder
import com.ryan.amg.review.processing.db.entity.ReviewProcessingEntity
import com.ryan.amg.review.processing.db.repository.ReviewProcessingRepository
import com.ryan.amg.review.processing.domain.ReviewProcessing
import com.ryan.amg.review.processing.domain.ReviewProcessingBuilder
import com.ryan.amg.review.processing.exception.ReviewNotFoundException
import spock.lang.Specification
import spock.lang.Unroll

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewProcessingServiceSpec extends Specification {

    ReviewProcessingRepository mockReviewProcessingRepository = Mock()

    ReviewProcessingService reviewProcessingService = new ReviewProcessingService(mockReviewProcessingRepository)

    def "Invoking createReviewProcessingRecord calls the repository and returns the expected domain object"() {

        given:
            ReviewProcessing inputReviewProcessing = new ReviewProcessingBuilder().build()

            ReviewProcessingEntity expectedRepositoryReviewProcessingEntity = new ReviewProcessingEntityBuilder().fromReviewProcessing(inputReviewProcessing).build()
            ReviewProcessingEntity mockedRepositoryReviewProcessingEntity = new ReviewProcessingEntityBuilder().fromReviewProcessing(inputReviewProcessing).build()

            ReviewProcessing expectedFinalReviewProcessing = new ReviewProcessingBuilder().build()

            1 * mockReviewProcessingRepository.save(_ as ReviewProcessingEntity) >> { args ->
                assertReflectionEquals(expectedRepositoryReviewProcessingEntity, (ReviewProcessingEntity) args.get(0))
                return mockedRepositoryReviewProcessingEntity
            }

        when:
            ReviewProcessing actualReviewProcessing = reviewProcessingService.createReviewProcessingRecord(inputReviewProcessing)

        then:
            assertReflectionEquals(expectedFinalReviewProcessing, actualReviewProcessing)

    }

    @Unroll
    def "Invoking deleteTagFromProcessingRecord retrieves the record from the database, removes the tag, and returns the expected domain object"() {

        given:
            String inputReviewId = 'ReviewId'
            String inputTagName = 'SomeTag'

            ReviewProcessingEntity mockedReadEntity = new ReviewProcessingEntityBuilder().withUnprocessedTags(inputTags.collect()).build()
            ReviewProcessingEntity expectedWriteEntity = new ReviewProcessingEntityBuilder().withUnprocessedTags(expectedTags.collect()).build()
            ReviewProcessingEntity mockedWriteEntity = new ReviewProcessingEntityBuilder().withUnprocessedTags(expectedTags.collect()).build()
            ReviewProcessing expectedReviewProcessing = new ReviewProcessingBuilder().fromReviewProcessingEntity(mockedWriteEntity).build()

            1 * mockReviewProcessingRepository.findById(inputReviewId) >> Optional.of(mockedReadEntity)

            1 * mockReviewProcessingRepository.save(_ as ReviewProcessingEntity) >> { args ->
                assertReflectionEquals(expectedWriteEntity, (ReviewProcessingEntity) args.get(0))
                return mockedWriteEntity
            }

        when:
            ReviewProcessing actualReviewProcessing = reviewProcessingService.deleteTagFromProcessingRecord(inputReviewId, inputTagName)

        then:
            assertReflectionEquals(expectedReviewProcessing, actualReviewProcessing)

        where:
            inputTags               | expectedTags
            ['SomeTag', 'OtherTag'] | ['OtherTag']
            ['SomeTag']             | []
            ['OtherTag']            | ['OtherTag']

    }

    def "Invoking deleteTagFromProcessingRecord throws a ReviewNotFoundException when the input review id is not found in the repository"() {

        given:
            1 * mockReviewProcessingRepository.findById('ReviewId') >> Optional.ofNullable(null)

        when:
            reviewProcessingService.deleteTagFromProcessingRecord('ReviewId', 'TagName')

        then:
            thrown(ReviewNotFoundException)

    }

    def "Invoking deleteReviewProcessingRecord calls the repository with the correct review id"() {

        given:
            String inputReviewId = 'id-123'

            1 * mockReviewProcessingRepository.deleteById(inputReviewId)

        when:
            reviewProcessingService.deleteReviewProcessingRecord(inputReviewId)

        then:
            noExceptionThrown()

    }

}
