package com.ryan.amg.review.processing.controller

import com.ryan.amg.review.processing.domain.ReviewProcessing
import com.ryan.amg.review.processing.domain.ReviewProcessingBuilder
import com.ryan.amg.review.processing.dto.ReviewProcessingCreateDTO
import com.ryan.amg.review.processing.dto.ReviewProcessingCreateDTOBuilder
import com.ryan.amg.review.processing.service.ReviewProcessingService
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewProcessingControllerSpec extends Specification {

    ReviewProcessingService mockReviewProcessingService = Mock()
    ReviewProcessingController reviewProcessingController = new ReviewProcessingController(mockReviewProcessingService)

    def "Invoking createReviewProcessingRecord calls the service layer and returns the expected domain object"() {

        given:
            String inputReviewId = 'reviewId-123'
            ReviewProcessingCreateDTO inputReviewProcessingCreateDTO = new ReviewProcessingCreateDTOBuilder().build()

            ReviewProcessing expectedServiceReviewProcessing = new ReviewProcessingBuilder().withReviewId(inputReviewId).withUnprocessedTags(inputReviewProcessingCreateDTO.getUnprocessedTags().collect()).build()
            ReviewProcessing mockedServiceReviewProcessing = new ReviewProcessingBuilder().withReviewId(inputReviewId).withUnprocessedTags(inputReviewProcessingCreateDTO.getUnprocessedTags().collect()).build()

            ReviewProcessing expectedFinalServiceReviewProcessing = new ReviewProcessingBuilder().withReviewId(inputReviewId).withUnprocessedTags(inputReviewProcessingCreateDTO.getUnprocessedTags().collect()).build()

            1 * mockReviewProcessingService.createReviewProcessingRecord(_ as ReviewProcessing) >> { args ->
                assertReflectionEquals(expectedServiceReviewProcessing, (ReviewProcessing) args.get(0))
                return mockedServiceReviewProcessing
            }

        when:
            ReviewProcessing actualReviewProcessing = reviewProcessingController.createReviewProcessingRecord(inputReviewId, inputReviewProcessingCreateDTO)

        then:
            assertReflectionEquals(expectedFinalServiceReviewProcessing, actualReviewProcessing)

    }

    def "Invoking deleteReviewProcessingRecord calls the service layer with the expected review id"() {

        given:
            String inputReviewId = 'reviewId-123'

            1 * mockReviewProcessingService.deleteReviewProcessingRecord(inputReviewId)

        when:
            reviewProcessingController.deleteReviewProcessingRecord(inputReviewId)

        then:
            noExceptionThrown()

    }

}
