package com.ryan.amg.review.processing.db.entity

import com.ryan.amg.review.processing.domain.ReviewProcessing

class ReviewProcessingEntityBuilder {
    String reviewId = 'id-123'
    List<String> unprocessedTags = ['tag1', 'tag2']

    ReviewProcessingEntityBuilder withReviewId(String reviewId) {
        this.reviewId = reviewId
        return this
    }

    ReviewProcessingEntityBuilder withUnprocessedTags(List<String> unprocessedTags) {
        this.unprocessedTags = unprocessedTags
        return this
    }

    ReviewProcessingEntityBuilder fromReviewProcessing(ReviewProcessing reviewProcessing) {
        this.reviewId = reviewProcessing.getReviewId()
        this.unprocessedTags = reviewProcessing.unprocessedTags.collect()
        return this
    }

    ReviewProcessingEntity build() {
        return new ReviewProcessingEntity(
            reviewId: reviewId,
            unprocessedTags: unprocessedTags
        )
    }
}
