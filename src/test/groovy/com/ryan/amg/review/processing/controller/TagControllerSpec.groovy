package com.ryan.amg.review.processing.controller

import com.ryan.amg.review.processing.domain.ReviewProcessing
import com.ryan.amg.review.processing.domain.ReviewProcessingBuilder
import com.ryan.amg.review.processing.service.ReviewProcessingService
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class TagControllerSpec extends Specification {

    ReviewProcessingService mockReviewProcessingService = Mock()
    TagController tagController = new TagController(mockReviewProcessingService)

    def "Invoking deleteTagFromProcessingRecord correctly invokes the service layer and returns the expected domain object"() {

        given:
            ReviewProcessing mockedReviewProcessing = new ReviewProcessingBuilder().build()
            ReviewProcessing expectedReviewProcessing = new ReviewProcessingBuilder().build()

            1 * mockReviewProcessingService.deleteTagFromProcessingRecord('ReviewId', 'TagName') >> mockedReviewProcessing

        when:
            ReviewProcessing actualReviewProcessing = tagController.deleteTagFromProcessingRecord('ReviewId', 'TagName')

        then:
            assertReflectionEquals(expectedReviewProcessing, actualReviewProcessing)

    }

}
