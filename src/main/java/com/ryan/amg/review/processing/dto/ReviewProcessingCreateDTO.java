package com.ryan.amg.review.processing.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewProcessingCreateDTO {
    private List<String> unprocessedTags;
}
