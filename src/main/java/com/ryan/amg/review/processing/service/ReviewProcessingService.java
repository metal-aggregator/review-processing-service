package com.ryan.amg.review.processing.service;

import com.ryan.amg.review.processing.db.entity.ReviewProcessingEntity;
import com.ryan.amg.review.processing.db.repository.ReviewProcessingRepository;
import com.ryan.amg.review.processing.domain.ReviewProcessing;
import com.ryan.amg.review.processing.exception.ReviewNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class ReviewProcessingService {

    public final ReviewProcessingRepository reviewProcessingRepository;

    public ReviewProcessing createReviewProcessingRecord(ReviewProcessing reviewProcessing) {
        return reviewProcessingRepository.save(new ReviewProcessingEntity(reviewProcessing)).toDomain();
    }

    public ReviewProcessing deleteTagFromProcessingRecord(String reviewId, String tagName) {
        Optional<ReviewProcessingEntity> reviewProcessingOptional = reviewProcessingRepository.findById(reviewId);
        ReviewProcessingEntity reviewProcessingEntity = reviewProcessingOptional.orElseThrow(() -> new ReviewNotFoundException(reviewId));
        reviewProcessingEntity.getUnprocessedTags().remove(tagName);
        return reviewProcessingRepository.save(reviewProcessingEntity).toDomain();
    }

    public void deleteReviewProcessingRecord(String reviewId) {
        reviewProcessingRepository.deleteById(reviewId);
    }

}
