package com.ryan.amg.review.processing.db.repository;

import com.ryan.amg.review.processing.db.entity.ReviewProcessingEntity;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface ReviewProcessingRepository extends CrudRepository<ReviewProcessingEntity, String> {}
