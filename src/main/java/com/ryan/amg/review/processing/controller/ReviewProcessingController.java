package com.ryan.amg.review.processing.controller;

import com.ryan.amg.review.processing.domain.ReviewProcessing;
import com.ryan.amg.review.processing.dto.ReviewProcessingCreateDTO;
import com.ryan.amg.review.processing.service.ReviewProcessingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews/{reviewId}/processing")
@AllArgsConstructor
public class ReviewProcessingController {

    private final ReviewProcessingService reviewProcessingService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ReviewProcessing createReviewProcessingRecord(@PathVariable String reviewId, @RequestBody ReviewProcessingCreateDTO reviewProcessingCreateDTO) {
        ReviewProcessing inputReviewProcessing = new ReviewProcessing(reviewId, reviewProcessingCreateDTO.getUnprocessedTags());
        return reviewProcessingService.createReviewProcessingRecord(inputReviewProcessing);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteReviewProcessingRecord(@PathVariable String reviewId) {
        reviewProcessingService.deleteReviewProcessingRecord(reviewId);
    }

}
