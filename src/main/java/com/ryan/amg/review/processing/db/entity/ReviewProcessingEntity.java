package com.ryan.amg.review.processing.db.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.ryan.amg.review.processing.domain.ReviewProcessing;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.List;

@DynamoDBTable(tableName = "Review_Processing")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReviewProcessingEntity {

    @Id
    @DynamoDBHashKey
    private String reviewId;

    @DynamoDBAttribute(attributeName = "unprocessed_tags")
    private List<String> unprocessedTags;

    public ReviewProcessingEntity(ReviewProcessing reviewProcessing) {
        this.reviewId = reviewProcessing.getReviewId();
        this.unprocessedTags = reviewProcessing.getUnprocessedTags();
    }

    public ReviewProcessing toDomain() {
        return new ReviewProcessing(reviewId, unprocessedTags);
    }

}
