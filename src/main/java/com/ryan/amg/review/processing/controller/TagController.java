package com.ryan.amg.review.processing.controller;

import com.ryan.amg.review.processing.domain.ReviewProcessing;
import com.ryan.amg.review.processing.service.ReviewProcessingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews/{reviewId}/processing/tags")
@AllArgsConstructor
public class TagController {

    private final ReviewProcessingService reviewProcessingService;

    @DeleteMapping(value = "/{tagName}")
    public ReviewProcessing deleteTagFromProcessingRecord(@PathVariable String reviewId, @PathVariable String tagName) {
        return reviewProcessingService.deleteTagFromProcessingRecord(reviewId, tagName);
    }

}
