package com.ryan.amg.review.processing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewProcessingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReviewProcessingServiceApplication.class, args);
    }

}
